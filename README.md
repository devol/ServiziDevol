# Servizi.devol.it
 
 Codice di [Servizi.devol.it](https://servizi.devol.it)
 
 ![hostux.network](https://hostux.pics/images/2019/12/05/2019-12-05_14-30c56e6b71475b8578.png)
 
## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)

## Thanks to

- https://libreops.cc for making the code of their website available
- https://hostux.network for making the code of their website available
- Federico for Italian translation
- Joseph for the Spanish translation
- [Suguru Hirahara](https://mstdn.progressiv.dev/@suguru) for the Japanese translation
- Users of the services offered
- [Donors](https://liberapay.com/valere_hostux/) who support my work.
